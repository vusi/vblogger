﻿namespace vBlogger.Web.AppSettings
{
	public class AppSetting
	{
		public int FeedSize { get; set; }
		public string ApiBaseUrl { get; set; }
		public string ImagesFolder { get; set; }
	}
}
