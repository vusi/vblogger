﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(vBlogger.Web.Areas.Identity.IdentityHostingStartup))]
namespace vBlogger.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}