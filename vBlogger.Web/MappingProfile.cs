﻿using AutoMapper;
using vBlogger.Models;
using vBlogger.Web.Models;

namespace vBlogger.Web
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<PostViewModel, PostDto>().ReverseMap();
		}
	}
}
