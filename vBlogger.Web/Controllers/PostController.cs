﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using vBlogger.Models;
using vBlogger.Web.AppSettings;
using vBlogger.Web.HttpClient;
using vBlogger.Web.Models;

namespace vBlogger.Web.Controllers
{
	public class PostController : Controller
	{
		private readonly IMapper _mapper;
		private readonly AppSetting _postsSettings;
		private readonly IPostHttpClient _postHttpClient;
		private readonly IHostingEnvironment _hostingEnvironment;

		public PostController(AppSetting postsSettings, IMapper mapper, IPostHttpClient postHttpClient, IHostingEnvironment environment)
		{
			_mapper = mapper;
			_postsSettings = postsSettings;
			_postHttpClient = postHttpClient;
			_hostingEnvironment = environment;
		}

		public IActionResult Index()
		{
			var postsModel = new List<PostViewModel>();
			return View(postsModel);
		}

		[HttpGet]
		public async Task<IActionResult> LoadPost(bool loadMore = false)
		{
			var postsModel = new List<PostViewModel>();

			var feedSize = _postsSettings.FeedSize;
			if (loadMore)
			{
				feedSize += _postsSettings.FeedSize;
			}
			var posts = await _postHttpClient.GetPosts(feedSize);
			if (posts.Any())
			{
				postsModel = _mapper.Map<List<PostViewModel>>(posts);
			}

			return PartialView("_PostsPartial", postsModel);
		}

		[HttpGet]
		public IActionResult Create()
		{
			return View();
		}

		[HttpPost]
		public async Task<IActionResult> Create(IFormFile picture, PostViewModel model)
		{
			var post = _mapper.Map<PostDto>(model);
			post.Tags = AddTags(post);
			post.ImageUrl = await GetImageUrl(picture);
			post.Author = User.Identity.Name;

			var created = await _postHttpClient.Create(post);
			return RedirectToAction("Manage");
		}

		public async Task<IActionResult> Manage()
		{
			var posts = await _postHttpClient.GetPosts(0);
			var postsModel = _mapper.Map<List<PostViewModel>>(posts);

			return View(postsModel);
		}

		public async Task<IActionResult> Edit(int id)
		{
			var post = await _postHttpClient.GetPostById(id);
			post.TagsString = string.Join(",", post.Tags.Select(x => x.Title));

			var postModel = _mapper.Map<PostViewModel>(post);
			return View(postModel);
		}

		public async Task<IActionResult> Details(int id)
		{
			var post = await _postHttpClient.GetPostById(id);
			post.TagsString = string.Join(",", post.Tags.Select(x => x.Title));

			var postModel = _mapper.Map<PostViewModel>(post);
			return View(postModel);
		}

		[HttpPost]
		public async Task<IActionResult> Edit(IFormFile picture, PostViewModel model)
		{
			var post = _mapper.Map<PostDto>(model);
			post.Tags = AddTags(post);
			post.ImageUrl = await GetImageUrl(picture);

			var updated = await _postHttpClient.Update(post);
			return RedirectToAction("Manage");
		}

		public async Task<IActionResult> Delete(int id)
		{
			var deleted = await _postHttpClient.Delete(id);
			return RedirectToAction("Manage");
		}

		private async Task<string> GetImageUrl(IFormFile picture)
		{
			var uploads = Path.Combine(_hostingEnvironment.WebRootPath, _postsSettings.ImagesFolder);
			var filePath = Path.Combine(uploads, picture.FileName);
			using (var fileStream = new FileStream(filePath, FileMode.Create))
			{
				await picture.CopyToAsync(fileStream);
			}
			return $"/{_postsSettings.ImagesFolder}/{picture.FileName}";
		}

		private IEnumerable<TagDto> AddTags(PostDto post)
		{
			return post.TagsString?.Split(',').ToList().Select(x => new TagDto { Title = x }).ToList();
		}
	}
}