﻿using System;
using System.Collections.Generic;

namespace vBlogger.Web.Models.MockData
{
	public static class ListOfPosts
	{
		public static List<PostViewModel> GetMockPosts(bool loadMore)
		{
			var posts = new List<PostViewModel>();
			int count = 0;
			for (var i = 0; i < 3; i++)
			{
				posts.Add(
					new PostViewModel
					{
						Title = "Article " + i,
						ImageUrl = "https://bit.ly/fcc-running-cats",
						Blurb =
							"My friend the bee keeper makes all his own honey now, and there’s really nothing like it! Divine on warm scones with homemade jam. Mmmm!"
							+ "As far as cats go… I’ve been thinking of writing a series of stories about each of my pusses. Some were noteworthy characters! A few, smarter than most people I know… once you learn to…",
						Author = "Author for article " + i,
						CreatedDate = DateTime.Now
					}
				);
				count++;
			}

			if (loadMore)
			{
				for (var i = 1; i < 4; i++)
				{
					posts.Add(
						new PostViewModel
						{
							Title = "Article " + int.Parse(count.ToString()+ i),
							ImageUrl = "https://bit.ly/fcc-running-cats",
							Blurb =
								"My friend the bee keeper makes all his own honey now, and there’s really nothing like it! Divine on warm scones with homemade jam. Mmmm!"
								+ "As far as cats go… I’ve been thinking of writing a series of stories about each of my pusses. Some were noteworthy characters! A few, smarter than most people I know… once you learn to…",
							Author = "Author for article " + count + i,
							CreatedDate = DateTime.Now
						}
					);
				}
			}
			return posts;
		}
	}
}
