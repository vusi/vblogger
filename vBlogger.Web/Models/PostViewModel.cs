﻿using System;

namespace vBlogger.Web.Models
{
    public class PostViewModel
    {
        public int Id { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string Blurb { get; set; }
        public string ImageUrl { get; set; }
        public string Article { get; set; }
        public string TagsString { get; set; }
		public DateTime CreatedDate { get; set; }
    }
}