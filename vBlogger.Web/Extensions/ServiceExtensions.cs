﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using vBlogger.Web.AppSettings;
using vBlogger.Web.Data;

namespace vBlogger.Web.Extensions
{
	public static class ServiceExtensions
	{
		public static void ConfigureAutoMapper(this IServiceCollection services)
		{
			var mappingConfig = new MapperConfiguration(configure =>
			{
				configure.AddProfile(new MappingProfile());
			});
			var mapper = mappingConfig.CreateMapper();
			services.AddSingleton(mapper);
		}

		public static void ConfigureDatabase(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddDbContext<ApplicationDbContext>(options =>
				options.UseSqlServer(
					configuration.GetConnectionString("BlogConnection")));
			services.AddDefaultIdentity<IdentityUser>()
				.AddDefaultUI(UIFramework.Bootstrap4)
				.AddEntityFrameworkStores<ApplicationDbContext>();
		}
			
		public static void ConfigureAppSettings(this IServiceCollection services, IConfiguration configuration)
		{
			var postsSettings = new AppSetting();
			configuration.GetSection(nameof(AppSetting)).Bind(postsSettings);
			services.AddSingleton(postsSettings);
		}
	}
}
