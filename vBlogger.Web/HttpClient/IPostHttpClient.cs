﻿using vBlogger.Models;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace vBlogger.Web.HttpClient
{
	public interface IPostHttpClient
	{
		Task<IEnumerable<PostDto>> GetPosts(int feedSize);
		Task<bool> Create(PostDto post);
		Task<PostDto> GetPostById(int id);
		Task<bool> Update(PostDto post);
		Task<bool> Delete(int id);
	}
}
