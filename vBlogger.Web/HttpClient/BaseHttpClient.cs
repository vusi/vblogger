﻿using vBlogger.Web.AppSettings;

namespace vBlogger.Web.HttpClient
{
	public abstract class BaseHttpClient
	{
		public string BaseUrl { get; set; }

		protected BaseHttpClient(AppSetting postsSettings)
		{
			BaseUrl = postsSettings.ApiBaseUrl;
		}
	}
}