﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using vBlogger.Models;
using vBlogger.Web.AppSettings;

namespace vBlogger.Web.HttpClient
{
	public class PostHttpClient : BaseHttpClient, IPostHttpClient
	{
		public PostHttpClient(AppSetting postsSettings) :
			base(postsSettings)
		{ }

		public async Task<IEnumerable<PostDto>> GetPosts(int feedSize)
		{
			try
			{
				IEnumerable<PostDto> posts = new List<PostDto>();
				using (var httpClient = new System.Net.Http.HttpClient())
				{
					var response = await httpClient.GetAsync($"{BaseUrl}/posts/{feedSize}");
					if (response.IsSuccessStatusCode)
					{
						var apiResponse = await response.Content.ReadAsStringAsync();
						posts = JsonConvert.DeserializeObject<IEnumerable<PostDto>>(apiResponse);
					}
					return posts;
				}
			}
			catch (Exception e)
			{
				throw;
			}
		}

		public async Task<bool> Create(PostDto post)
		{
			try
			{
				var result = false;
				using (var httpClient = new System.Net.Http.HttpClient())
				{
					var content = new StringContent(JsonConvert.SerializeObject(post), Encoding.UTF8, "application/json");
					using (var response = await httpClient.PostAsync($"{BaseUrl}/create", content))
					{
						var apiResponse = await response.Content.ReadAsStringAsync();
						result = JsonConvert.DeserializeObject<bool>(apiResponse);
					}
				}
				return result;
			}
			catch (Exception e)
			{
				throw;
			}
		}

		public async Task<PostDto> GetPostById(int id)
		{
			try
			{
				var post = new PostDto();
				using (var httpClient = new System.Net.Http.HttpClient())
				{
					var response = await httpClient.GetAsync($"{BaseUrl}/post/{id}");
					if (response.IsSuccessStatusCode)
					{
						var apiResponse = await response.Content.ReadAsStringAsync();
						post = JsonConvert.DeserializeObject<PostDto>(apiResponse);
					}
					return post;
				}
			}
			catch (Exception e)
			{
				throw;
			}
		}

		public async Task<bool> Update(PostDto post)
		{
			try
			{
				var result = false;
				using (var httpClient = new System.Net.Http.HttpClient())
				{
					var content = new StringContent(JsonConvert.SerializeObject(post), Encoding.UTF8, "application/json");
					using (var response = await httpClient.PutAsync($"{BaseUrl}/update", content))
					{
						var apiResponse = await response.Content.ReadAsStringAsync();
						result = JsonConvert.DeserializeObject<bool>(apiResponse);
					}
				}
				return result;
			}
			catch (Exception e)
			{
				throw;
			}
		}

		public async Task<bool> Delete(int id)
		{
			try
			{
				var result = false;
				using (var httpClient = new System.Net.Http.HttpClient())
				{
					using (var response = await httpClient.DeleteAsync($"{BaseUrl}/delete/{id}"))
					{
						var apiResponse = await response.Content.ReadAsStringAsync();
						result = JsonConvert.DeserializeObject<bool>(apiResponse);
					}
				}
				return result;
			}
			catch (Exception e)
			{
				throw;
			}
		}


	}
}
