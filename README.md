# vBlogger

vBlogger is an application built for knowledge purposes. 

## Application Structure

This basic blogging application has a simple technology stack, consisting of:

```bash
> MS SQL Server
> Entity Framework Core
> ASP.NET Core Web Api (Documented with Swashbuckle)
> ASP.NET MVC Core
```


## Installation

To get started. Clone the code base

```bash
> git clone https://gitlab.com/vusi/vblogger.git
```

## Usage (Command Line)

Upon running the API, the migrate scripts will be run and it will try to create the database on the SQL Server localhost instance

**Web API**
```bash
> cd vblogger/vblogger.api
> dotnet restore
> dotnet build
> dotnet run

> navigate to: 'https://localhost:5001/swagger/index.html'
```

**Web App**
```bash
> cd vblogger/vblogger.web
> dotnet restore
> dotnet build
> dotnet run

> navigate to: 'https://localhost:2001/'
```

## Usage (Visual Studio)

**Web API**
```bash
> Run vBlogger solution
> Set solution to start multiple projects (vBlogger.Api & vBlogger.Web)
> Start debugging (F5)
```

## Known Issues

```python
> Anybody can create an account and see other people's posts :) 
```


## License
[MIT](https://choosealicense.com/licenses/mit/)