import { Component, OnInit } from '@angular/core';
import { Blog } from '../../_interfaces/blogs.model';
import { HttpclientService } from './../../shared/services/httpclient.service';

@Component({
  selector: 'app-blogs-list',
  templateUrl: './blogs-list.component.html',
  styleUrls: ['./blogs-list.component.css']
})
export class BlogsListComponent implements OnInit {

  public blogs: Blog[]

  constructor(private repository: HttpclientService) { }

  ngOnInit() {
    this.getAllBlogs();
  }

  getAllBlogs(){
    let apiAddress: string = "api/blogs";
    this.repository.getData(apiAddress)
    .subscribe(res => {
      this.blogs = res as Blog[];
    })
  }
}
