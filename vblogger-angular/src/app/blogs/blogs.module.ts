import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BlogsListComponent } from './blogs-list/blogs-list.component';

@NgModule({
  declarations: [BlogsListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: 'list', component: BlogsListComponent }
    ])
  ]
})
export class BlogsModule { }
