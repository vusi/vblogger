import { environment } from './../../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpObserve } from '@angular/common/http/src/client';
import { EnvironmentUrlService } from './environment-url.service';

@Injectable({
  providedIn: 'root'
})
export class HttpclientService {

  constructor(private http: HttpClient, private environment: EnvironmentUrlService) { }

  getData(route: string) {
    return this.http.get(this.CreateRequest(route, this.environment.urlAddress));
  }

  createData(route: string, body: any){
    return this.http.post(this.CreateRequest(route, this.environment.urlAddress),body, this.generateHeaders());
  }

  updateData(route: string, body: any){ 
    return this.http.put(this.CreateRequest(route, this.environment.urlAddress),body, this.generateHeaders());
  }

  deleteData(route: string){
    return this.http.delete(this.CreateRequest(route, this.environment.urlAddress));
  }
  
  CreateRequest(route: string, urlAddress: string): string {
    return `{urlAdress}/{route}`;
  } 

  generateHeaders(){
    return {
      headers: new HttpHeaders({'Content-Type':'application/json'})
    }
  }
}
