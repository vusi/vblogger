export interface Blog {
    id: string,
    title: string,
    article: string,
    tags: string,
    createdDate: string,
    blurb: string,
    author: string
}