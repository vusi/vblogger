import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  notFoundMessage = `404 SORRY WE COULND'T FIND WHAT YOU LOOKING FOR!!!`;
  
  constructor() { }

  ngOnInit() {
  }

}
