import { Component, OnInit } from '@angular/core';
import { BlogsService } from '../services/blogs.service'
import { from } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  homeString:string;
  posts = [];

  constructor(private blogService: BlogsService) { }

  ngOnInit() {
    this.homeString = "Welcome to vBlogger";    
    this.posts = this.blogService.getPosts();
  } 
}
