﻿using System;
using System.Collections.Generic;

namespace vBlogger.Models
{
    public class PostDto
    {
	    public PostDto()
	    {
		    Tags = new List<TagDto>();
	    }

        public int Id { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string Blurb { get; set; }

        public DateTime CreatedDate { get; set; }
		public string ImageUrl { get; set; }
        public string Article { get; set; }
        public IEnumerable<TagDto> Tags { get; set; }
        public string TagsString { get; set; }
    }
}