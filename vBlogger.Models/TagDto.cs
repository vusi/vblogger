﻿namespace vBlogger.Models
{
    public class TagDto
    {
        public string Title { get; set; }
    }
}