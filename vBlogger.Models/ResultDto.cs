﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vBlogger.Models
{
	public class ResultDto<T> where T : class
	{
		public bool IsSuccess { get; set; }
		public string Errors { get; set; }
		public T Data { get; set; }
	}
}
