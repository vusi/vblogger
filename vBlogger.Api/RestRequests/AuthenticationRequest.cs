﻿namespace vBlogger.Api.RestRequests
{
    public class AuthenticationRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
