﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using vBlogger.Api.Data.Entities;

namespace vBlogger.Api.Repositories
{
	public interface IPostRepository
	{
		Task<IEnumerable<Post>> GetPosts(int feedSize);
		Task<Post> GetPostById(int id);
		Task<bool> SavePost(Post post);
		Task<bool> UpdatePost(Post post);
		Task<bool> DeletePost(int id);
	}
}
