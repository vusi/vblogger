﻿using System.Threading.Tasks;
using vBlogger.Api.RestResponses;

namespace vBlogger.Api.Repositories
{
    public interface ISecurityRepository
    {
        Task<AuthenticationResponse> AuthenticateAsync(string username, string password);
        Task<AuthenticationResponse> RegisterAsync(string username, string password);
    }
}