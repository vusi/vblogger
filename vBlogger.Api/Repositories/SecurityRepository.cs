﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using vBlogger.Api.Extensions;
using vBlogger.Api.RestResponses;
using vBlogger.Api.Settings;
using vBlogger.Api.Utils;

namespace vBlogger.Api.Repositories
{
    public class SecurityRepository : ISecurityRepository
    {
        private readonly JwtSettings _jwtSettings;
        private readonly UserManager<IdentityUser> _userManager;

        public SecurityRepository(UserManager<IdentityUser> userManager, JwtSettings jwtSettings)
        {
            _userManager = userManager;
            _jwtSettings = jwtSettings;
        }

        public async Task<AuthenticationResponse> RegisterAsync(string username, string password)
        {
            var exists = await _userManager.FindByEmailAsync(username);
            if (exists != null)
            {
                return AuthenticationFailure(false,Errors.UserAlreadyExists);
            }

            var user = new IdentityUser {UserName = username, Email = username};
            var created = await _userManager.CreateAsync(user, password);
            if (!created.Succeeded)
            {
                
            }

            var token =  JwtExtensions.GenerateJwtToken(_jwtSettings.Secret, user);
            return new AuthenticationResponse { IsSuccess = true, JwtToken = token };
        }

        public async Task<AuthenticationResponse> AuthenticateAsync(string username, string password)
        {
	        try
	        {
		        var user = await _userManager.FindByEmailAsync(username);
		        if (user == null)
		        {
			        return AuthenticationFailure(false, Errors.InvalidUser);
		        }

		        var passwordCorrect = await IsPasswordCorrect(user, password);
		        if (!passwordCorrect)
		        {
			        return AuthenticationFailure(false, Errors.InvalidCredentials);
		        }

		        var token = JwtExtensions.GenerateJwtToken(_jwtSettings.Secret, user);
		        return new AuthenticationResponse {IsSuccess = true, JwtToken = token};
	        }
	        catch (Exception e)
	        {
		        throw e;
	        }
        }

        private async Task<bool> IsPasswordCorrect(IdentityUser user, string password)
        {
            return await _userManager.CheckPasswordAsync(user, password);
        }

        private AuthenticationResponse AuthenticationFailure(bool success, string errorMessage)
        {
            return new AuthenticationResponse { IsSuccess = success, ErrorMessage = errorMessage };
        }        
    }
}