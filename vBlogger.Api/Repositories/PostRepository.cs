﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using vBlogger.Api.Data;
using vBlogger.Api.Data.Entities;

namespace vBlogger.Api.Repositories
{
	public class PostRepository : IPostRepository
	{
		private readonly BlogDbContext _dbContext;
		public PostRepository(BlogDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public async Task<bool> SavePost(Post post)
		{
			_dbContext.Posts.Add(post);
			var saved = await _dbContext.SaveChangesAsync();
			return saved > 0;
		}

		public async Task<bool> UpdatePost(Post post)
		{
			try
			{
				_dbContext.Entry(post).State = EntityState.Modified;
				var updated = await _dbContext.SaveChangesAsync();
				return updated > 0;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public async Task<bool> DeletePost(int id)
		{
			var post = _dbContext.Posts.SingleOrDefault(p => p.Id == id);
			if (post == null)
			{
				return false;
			}

			_dbContext.Posts.Remove(post);
			var deleted = await _dbContext.SaveChangesAsync();
			return deleted > 0;
		}

		public async Task<IEnumerable<Post>> GetPosts(int feedSize)
		{
			if (feedSize > 0)
			{
				return await _dbContext.Posts.Take(feedSize).ToListAsync();
			}
			return await _dbContext.Posts.ToListAsync();
		}

		public async Task<Post> GetPostById(int id)
		{
			return await _dbContext.Posts.Include(t => t.Tags).SingleOrDefaultAsync(p => p.Id == id);
		}
	}
}