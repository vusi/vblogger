﻿namespace vBlogger.Api.RestResponses
{
    public class AuthenticationResponse
    {
        public bool IsSuccess { get; set; }
        public string JwtToken { get; set; }
        public string ErrorMessage { get; set; }
    }
}
