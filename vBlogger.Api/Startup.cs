﻿using vBlogger.Api.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using vBlogger.Api.Data;
using vBlogger.Api.Settings;

namespace vBlogger.Api
{
    public class Startup
    {
        private IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureIoC();
            services.ConfigureDatabase(Configuration);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.ConfigureSwagger(Configuration);
            services.ConfigureJwt(Configuration);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
	        UpdateDatabase(app);

			if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            var swagger = new SwaggerSettings();
            Configuration.GetSection(nameof(SwaggerSettings)).Bind(swagger);
            
            app.UseSwagger(setting => setting.RouteTemplate = swagger.JsonRoute);
            app.UseSwaggerUI(setting =>
            {
                setting.SwaggerEndpoint(swagger.UiEndPoint, swagger.Description);
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvc();
        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
			using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
			{
				var context = serviceScope.ServiceProvider.GetService<BlogDbContext>();
				context.Database.Migrate();
			}
		}
	}
}
