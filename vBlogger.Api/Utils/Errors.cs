﻿namespace vBlogger.Api.Utils
{
    public class Errors
    {
        public static string InvalidUser = "User does not exist.";
        public static string UserAlreadyExists = "User with this email already exists";
        public static string InvalidCredentials = "Internal server error";
    }
}