﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using vBlogger.Api.Data.Entities;
using vBlogger.Api.Repositories;

namespace vBlogger.Api.Controllers
{
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PostController : Controller
    {
	    private readonly IPostRepository _blogRepository;
	    public PostController(IPostRepository blogRepository)
	    {
		    _blogRepository = blogRepository;
	    }

	    [HttpGet("api/posts/{feedSize}")]
        public async Task<IActionResult> GetPosts(int feedSize)
        {
	        var posts = await _blogRepository.GetPosts(feedSize);
	        if (!posts.Any())
	        {
		        return NotFound("No Posts Exists");
	        }
	        return Ok(posts);
        }

        [HttpGet("api/post/{id}")]
        public async Task<IActionResult> GetPostById(int id)
        {
	        var post = await _blogRepository.GetPostById(id);
	        if (post == null)
	        {
		        return NotFound("Post not found");
	        }
	        return Ok(post);
        }

        [HttpPost("api/create")]
        public async Task<IActionResult> Create([FromBody] Post post)
        {
	        var success = await _blogRepository.SavePost(post);
	        if (!success)
	        {
		        return BadRequest("Failed to create this post");
	        }
	        return Ok(true);
        }

        [HttpPut("api/update")]
        public async Task<IActionResult> Update([FromBody] Post post)
        {
	        var success = await _blogRepository.UpdatePost(post);
	        if (!success)
	        {
		        return BadRequest("Failed to update this post");
	        }
	        return Ok(true);
        }

        [HttpDelete("api/delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
	        var success = await _blogRepository.DeletePost(id);
	        if (!success)
	        {
		        return BadRequest("Failed to delete this post");
	        }
	        return Ok(true);
        }
	}
}
