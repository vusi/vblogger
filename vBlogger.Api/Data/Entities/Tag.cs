﻿using System.ComponentModel.DataAnnotations;

namespace vBlogger.Api.Data.Entities
{
    public class Tag
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
    }
}