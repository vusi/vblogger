﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace vBlogger.Api.Data.Entities
{
    public class Post
    {
        [Key]
        public int Id { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string Blurb { get; set; }
        public string ImageUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Article { get; set; }
        public ICollection<Tag> Tags { get; set; }
    }
}