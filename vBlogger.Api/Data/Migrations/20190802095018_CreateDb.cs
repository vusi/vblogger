﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace vBlogger.Api.Data.Migrations
{
	public partial class CreateDb : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.CreateTable(
				name: "Posts",
				columns: table => new
				{
					Id = table.Column<int>(nullable: false)
						.Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
					Title = table.Column<string>(nullable: true),
					Author = table.Column<string>(nullable: true),
					CreatedDate = table.Column<DateTime>(nullable: true),
					Blurb = table.Column<string>(nullable: true),
					ImageUrl = table.Column<string>(nullable: true),
					Article = table.Column<string>(nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Posts", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "Tags",
				columns: table => new
				{
					Id = table.Column<int>(nullable: false)
						.Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
					Title = table.Column<string>(nullable: true),
					PostId = table.Column<int>(nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Tags", x => x.Id);
					table.ForeignKey(
						name: "FK_Tags_Posts_PostId",
						column: x => x.PostId,
						principalTable: "Posts",
						principalColumn: "Id",
						onDelete: ReferentialAction.Restrict);
				});

			migrationBuilder.InsertData(
				table: "Posts",
				columns: new[] { "Id", "Title", "Author", "CreatedDate", "Blurb", "ImageUrl", "Article" },
				values: new object[] { 1, "Why cats are awesome!","Batman",DateTime.Now, "All they need is to eat, sleep, and hang out. A lot of people don't have time to be taking their pets on walks every day", "/posts_images/cat.jpg	",
					"10 Reasons Why Cats Are Awesome"
					+"Updated on June 5, 2019"
					+"Jessica Smetz profile imageJust Ask Jess  moreContact Author"
					+"Why I Love Cats+"
					+"Source"
					+"I have always been an animal lover since I was a little kid. Unlike humans, animals give you unconditional love. They don’t dump you or leave you in the dirt. I love all animals, but I have always had a special love for cats.Cats are awesome, just look at YouTube! Only cats are hilarious enough to be the biggest celebrities on the web.What is it about cats that makes them cool ? It's probably that they pretty much take care of themselves!+"
					+"Here are the reasons why cats rule:"
					+"1.They are independent: Cats can be left by themselves for a while.You can take a short vacation and leave them at home.All they need is enough food and water to make it through. They don’t get separation anxiety and tear up your entire house like dogs do."
					+"2.They can use the bathroom: Yeah, litter boxes can be gross, but they are much easier to deal with than poop scattered all over the house.Cats can even be taught to be potty trained on a real toilet.How cool is that ?"
					+"3.They don’t need exercise: Cats don’t need to be taken on walks, or let outside.All they need is to eat, sleep, and hang out. A lot of people don’t have time to be taking their pets on walks every day."
					+"4.They are good with kids: The majority of cats are good with kids, and let kids carry them around.A few cats don't do well with kids, but you never have to worry about the kids getting injured or killed by a cat like you do with dogs." });


			migrationBuilder.InsertData(
				table: "Tags",
				columns: new[] { "Id", "Title", "PostId" },
				values: new object[] { 1, "Cats", 1 });

			migrationBuilder.CreateIndex(
				name: "IX_Tags_PostId",
				table: "Tags",
				column: "PostId");
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropTable(
				name: "Tags");

			migrationBuilder.DropTable(
				name: "Posts");
		}
	}
}
